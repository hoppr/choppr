## [0.1.3-dev.2](https://gitlab.com/hoppr/choppr/compare/v0.1.3-dev.1...v0.1.3-dev.2) (2024-12-05)


### Bug Fixes

* **deps:** update dependency hoppr to v1.13.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0a1c353](https://gitlab.com/hoppr/choppr/commit/0a1c35347cb9ec4eb2636f72c9094e73f406d537))

## [0.1.3-dev.1](https://gitlab.com/hoppr/choppr/compare/v0.1.2...v0.1.3-dev.1) (2024-12-04)


### Bug Fixes

* **deps:** update dependency pydantic-yaml to v1.4.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ef5a096](https://gitlab.com/hoppr/choppr/commit/ef5a0965e9ef3bf6bec566f9e7f16b52ef44f6bb))

## [0.1.3-dev.1](https://gitlab.com/hoppr/choppr/compare/v0.1.2-dev.1...v0.1.2-dev.2) (2024-11-12)


### Bug Fixes

* **deps:** update dependency pydantic-yaml to v1.4.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ef5a096](https://gitlab.com/hoppr/choppr/commit/ef5a0965e9ef3bf6bec566f9e7f16b52ef44f6bb))

## [0.1.2](https://gitlab.com/hoppr/choppr/compare/v0.1.1...v0.1.2) (2024-10-28)


### Bug Fixes

* choppr init versioning ([93113f4](https://gitlab.com/hoppr/choppr/commit/93113f48f8c6cfdea8a64ab9501637fd2d84f92d))

## [0.1.2-dev.1](https://gitlab.com/hoppr/choppr/compare/v0.1.1...v0.1.2-dev.1) (2024-10-28)


### Bug Fixes

* choppr init versioning ([93113f4](https://gitlab.com/hoppr/choppr/commit/93113f48f8c6cfdea8a64ab9501637fd2d84f92d))

## [0.1.1](https://gitlab.com/hoppr/choppr/compare/v0.1.0...v0.1.1) (2024-10-24)


### Bug Fixes

* base python rules ([13f57c3](https://gitlab.com/hoppr/choppr/commit/13f57c31d80a70f1f1e4c0cb1385eb8342b57b11))

## [0.1.0-dev.2](https://gitlab.com/hoppr/choppr/compare/v0.1.0-dev.1...v0.1.0-dev.2) (2024-10-24)


### Bug Fixes

* base python rules ([13f57c3](https://gitlab.com/hoppr/choppr/commit/13f57c31d80a70f1f1e4c0cb1385eb8342b57b11))

## [0.1.0](https://gitlab.com/hoppr/choppr/compare/v0.0.0...v0.1.0) (2024-10-21)


### Features

* Initial release ([c61fe48](https://gitlab.com/hoppr/choppr/commit/c61fe48ba377090a598d1ccd9894e4f036e7c54d))
* Initial release with image ([3652304](https://gitlab.com/hoppr/choppr/commit/36523042ed9c85f316f5b793c6259d2866cd4ff7))

## [0.1.0-dev.1](https://gitlab.com/hoppr/choppr/compare/v0.0.0...v0.1.0-dev.1) (2024-10-21)


### Features

* Initial release ([c61fe48](https://gitlab.com/hoppr/choppr/commit/c61fe48ba377090a598d1ccd9894e4f036e7c54d))
* Initial release with image ([3652304](https://gitlab.com/hoppr/choppr/commit/36523042ed9c85f316f5b793c6259d2866cd4ff7))
