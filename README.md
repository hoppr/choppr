<div align="center">
  <img src="https://gitlab.com/hoppr/choppr/-/raw/wip/media/choppr_the_crocodile.svg?ref_type=heads" width="500"/>
</div>

# Choppr

A Hoppr plugin to filter unused components out of the delivered SBOM using strace results.

Choppr refines the components in a [Software Bill of Materials (SBOM)](https://en.wikipedia.org/wiki/Software_supply_chain).  It does not replace SBOM generation tools.  Mainly, Choppr analyses a build or runtime to verify which components are used, and remove the SBOM components not used.  Starting with file accesses, it works backwards from how an SBOM generation tool typically would.  For example SBOM generators use the yum database to determine which packages yum installed.  Choppr looks at all the files accessed and queries sources like yum to determine the originating package.

Other intended results include:
- Reducing installed components.  Size is optimized.  The number of vulnerabilities is reduced.  The less tools available to an attacker the better.
- Creating a runtime container from the build container
- Detecting files without corresponding SBOM components

# Configuration

## manifest.yml
You must list the RPM repositories used on your system in the [`manifest.yml`](https://hoppr.dev/docs/using-hoppr/input-files/manifest) file, for example:

```yml
repositories:
  rpm:
    - url: http://mirrorlist.rockylinux.org/?arch=x86_64&repo=BaseOS-8
    - url: http://mirrorlist.rockylinux.org/?arch=x86_64&repo=AppStream-8
    - url: https://mirrors.rockylinux.org/powertools/rocky/8/
    - url: https://mirrors.rockylinux.org/extra/rocky/8/
    - url: https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
```

To obtain this list, use the following command:

```bash
# For RHEL 8 and later
dnf config-manager --show-repos
# or
dnf repolist --verbose

# For RHEL 7 and earlier
yum repolist --verbose
```

With the output from one of these commands, you should be able to find the URLs to the repositories used on your system.

## transfer.yml
You must add choppr as a plugin and configure it in the [`transfer.yml`](https://hoppr.dev/docs/using-hoppr/input-files/transfer) file, for example:

```yml
Filter:
  plugins:
    - name: choppr.plugin
    config:
      strace_results: strace_output.txt
      certificates:
        - url: my.privaterepo.com
          certificate: /certs/combined.pem
      strace_regex_excludes:
        - ^.*<project-name>.*$
        - ^.*\.(c|cpp|cxx|h|hpp|o|py|s)$
        - ^/usr/share/pkgconfig$
        - ^/tmp$
        - ^bin$
        - ^.*\.git.*$
        - ^.*(\.\.)+.*$
        - ^.*(CMakeFiles.*|\.cmake)$
```

## Required Configuration Variables

### strace_results

The path to the output file created when running strace on your build or runtime executable.

This file can be creating using the following command to wrap your build script or runtime executable.

```bash
strace -f -e trace=file -o "strace_output.txt" "<build script/runtime executable>"
```

**Type:** str

**Example Usage:**
```yml
strace_results: strace_output.txt
```

## Optional Configuration Variables

### cache_dir

The path for the cache directory where Choppr will output temporary and downloaded files.

**Default:** ./.cache/choppr

**Type:** str

**Example Usage:**
```yml
cache_dir: /tmp/choppr
```

### certificates

A list of objects with a url and certificate key that is used to access the provided url when a self signed certificate needs to be used.

**Default:** []

**Type:** list[dict[str, str]]

**Example Usage:**
```yml
certificates:
  - url: my.privaterepo.com
    certificate: /certs/combined.pem
```

### clear_cache

Enable `clear_cache` to delete the cache directory when Choppr finishes running.

**Default:** false

**Type:** bool

**Example Usage:**
```yml
clear_cache: true
```

### delete_excluded

Disable `delete_excluded` to keep RPMs that are discovered to be unnecessary and marked as excluded.

**Default:** true

**Type:** bool

**Example Usage:**
```yml
delete_excluded: false
```

### strace_regex_excludes

An array of regex strings, used to filter the strace input.  The example below shows some of the recommended regular expressions.

**Default:** []

**Type:** list[str]

**Example Usage:**
```yml
strace_regex_excludes:
  - "^.*project-name.*$"              # Ignore all files related to your source, in general, by excluding any path containing the project name
  - "^.*\.(c|cpp|cxx|h|hpp|o|py|s)$"  # Ignore source, header, object, and script files
  - "^/usr/share/pkgconfig$"          # Ignore pkgconfig, which is included/modified by several RPMs
  - "^/tmp$"                          # Ignore the tmp directory
  - "^bin$"                           # Ignore overly simple files, that will be matched by most RPMs
  - "^.*\.git.*$"                     # Ignore all hidden git directories and files
  - "^.*(\.\.)+.*$"                   # Ignore all relative paths containing '..'
  - "^.*(CMakeFiles.*|\.cmake)$"      # Ignore all CMake files
```

# Generating strace

# Approaches

How to use Choppr depends on your project and needs.  Consider the following use cases and their recommended approaches.  Note, this references [CISA defined SBOM types](https://www.cisa.gov/sites/default/files/2023-04/sbom-types-document-508c.pdf).


## Build SBOM of software product

The user provides the required content.  Choppr determines which comoponents were used during the build.  The exclude list tells Choppr to remove components like CMake, because the user is certain no CMake software was built into their product.  An uninstall script is generated.  Building again after removing these components verifies no required components were lost.

## Create runtime image and Runtime SBOM from build image

Choppr uses a multistage build to `ADD` the files used.  Optionally metadata such as the yum database can be kept.  The additional include list can be used to specify dynamically linked libraries, necessary services, or any other necessary components that were not exercised during build.  This will also be reflected in the SBOM components.

## Create Runtime SBOM from runtime image

Similar to analyzing a build, Choppr can analyze a runtime.  Note, to if this is used to describe a delivery, it should be meged with the Build SBOM.