"""Module with decorator functions."""

from __future__ import annotations

import functools

from typing import TYPE_CHECKING, Any, TypeVar


if TYPE_CHECKING:
    from collections.abc import Callable


__all__ = ["limit_recursion"]
__author__ = "Jesse Boswell <jesse.a.boswell@lmco.com>"
__copyright__ = "Copyright (C) 2024 Lockheed Martin Corporation"
__license__ = "MIT License"

T = TypeVar("T")


def limit_recursion(max_depth: int) -> Callable[[Callable[..., T]], Callable[..., T | None]]:
    """Limit the recursion depth of a function.

    Returns None instead of the function result if the limit is reached.

    Arguments:
        max_depth: The maximum allowed recursion depth.

    Returns:
        A decorator function that wraps the original function.
    """

    def decorator(func: Callable[..., T]) -> Callable[..., T | None]:
        @functools.wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> T | None:  # noqa: ANN401
            wrapper.depth = getattr(wrapper, "depth", 0) + 1  # type: ignore[attr-defined]
            return None if wrapper.depth > max_depth else func(*args, **kwargs)  # type: ignore[attr-defined]

        return wrapper

    return decorator
