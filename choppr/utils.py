"""Utility functions used within the Choppr plugin.

Functions:
    - append_slash
    - http_get
    - remove_parenthesis

Classes:
    - RPM
"""

from __future__ import annotations

import re
import time

from pathlib import Path
from typing import TYPE_CHECKING, Final

import jmespath
import requests


if TYPE_CHECKING:
    from pydantic import HttpUrl
    from requests import Response
    from requests.auth import HTTPBasicAuth

    from choppr.types import PackageData


__all__ = ["http_get", "prepend_slash", "remove_parenthesis"]
__author__ = "Jesse Boswell <jesse.a.boswell@lmco.com>"
__copyright__ = "Copyright (C) 2024 Lockheed Martin Corporation"
__license__ = "MIT License"


REQUEST_RETRIES: Final[int] = 3
REQUEST_RETRY_INTERVAL: Final[float] = 5
REQUEST_TIMEOUT: Final[float] = 60


def http_get(url: HttpUrl, auth: HTTPBasicAuth | None = None, verify: str | bool = True) -> Response:
    """Perform and HTTP get request with the provided parameters.

    Arguments:
        url: The URL to access
        auth: The credentials needed to access the URL (default None)
        verify: The certificate needed to access the URL (default True)

    Returns:
        Response: The response from the request
    """
    for attempt in range(REQUEST_RETRIES):
        if attempt > 0:
            time.sleep(REQUEST_RETRY_INTERVAL)

        response = requests.get(
            url,
            auth=auth,
            allow_redirects=False,
            stream=True,
            verify=verify,
            timeout=REQUEST_TIMEOUT,
        )
        if response.ok:
            return response

    return response


def prepend_slash(text: str) -> str:
    """Prepend a slash to a string that doesn't have one.

    Arguments:
        text: A string to prepend a slash to

    Returns:
        str: The string with a prepended slash
    """
    return f"/{text.removeprefix('/')}"


def remove_parenthesis(text: str) -> str:
    """Remove text within parenthesis, to include empty parenthesis.

    Arguments:
        text: Text to remove parenthesis from

    Returns:
        str: Text with parenthesis removed
    """
    return re.sub(r"\(.*?\)", "", text)


class RPM:
    """Utility methods to analyze RPM packages."""

    @staticmethod
    def search_files_section(file_path: str, package_data: set[PackageData]) -> set[PackageData]:
        """Search the files secetion of a given package for a given file.

        It will search files for an exact match, and for directories, it will check if the given
        file path starts with the directory path.

        Arguments:
            file_path: The file to search for
            package_data: The package to search

        Returns:
            set[PackageData]: Packages containing the file
        """
        # Search files
        providers: set[PackageData] = {
            data
            for data in package_data
            if jmespath.search(
                expression=(f"format.file[?type(@) == 'string' && @ == '{file_path}']"),
                data=data.package,
            )
        }
        if not providers:
            # Search directories
            for data in package_data:
                directories: list[str] | None = jmespath.search(
                    expression='format."file"[?"@type" == \'dir\']."#text"',
                    data=data.package,
                )
                if directories and any(file_path.startswith(directory) for directory in directories):
                    providers.add(data)

        return providers

    @staticmethod
    def search_rpm_provides_section(file_path: str, package_data: set[PackageData]) -> set[PackageData]:
        """Search the rpm:provides secetion of a given package for a given file.

        Arguments:
            file_path: The file to search for
            package_data: The package to search

        Returns:
            set[PackageData]: Packages containing the file
        """
        providers: set[PackageData] = set()

        for data in package_data:
            # Search provides section
            package_provides: list[str] | None = jmespath.search(
                expression=('format."rpm:provides"."rpm:entry"[*]."@name"'),
                data=data.package,
            )
            if package_provides and any(
                remove_parenthesis(provides) == Path(file_path).name for provides in package_provides
            ):
                providers.add(data)

        return providers
