"""The types used in conjunction with Choppr."""

from __future__ import annotations

import re

from hashlib import sha256
from pathlib import Path
from typing import Any

import jmespath

from pydantic import BaseModel, Field, PrivateAttr, validator

from choppr import strace


__all__ = ["PackageData"]
__author__ = "Jesse Boswell <jesse.a.boswell@lmco.com>"
__copyright__ = "Copyright (C) 2024 Lockheed Martin Corporation"
__license__ = "MIT License"


def _validate_file_path(value: str) -> Path:
    path = Path(value)

    if not path.is_file():
        raise ValueError(f"File does not exist: {path}")
    return path


class Certificate(BaseModel):
    # Required Attributes
    url: str
    certificate: Path

    # Validators
    _validate_certificate = validator("certificate", pre=True, allow_reuse=True)(_validate_file_path)


class ChopprConfig(BaseModel):
    """Class to validate and parse the configuration values provided to ChopprPlugin.

    Members:
        - strace_results
        - cache_dir
        - certificates
        - clear_cache
        - delete_excluded
        - strace_regex_excludes

    Methods:
        - strace_files
    """

    # Required Attributes
    strace_results: Path
    # Optional Attributes
    cache_dir: Path = Field(default_factory=lambda: Path.cwd() / ".cache" / "choppr")
    certificates: dict[str, Path] = Field(default={})
    clear_cache: bool = False
    delete_excluded: bool = True
    strace_regex_excludes: list[re.Pattern] = Field(default=[])
    # Private Attributes
    _strace_files: set[str] = PrivateAttr(default=set())

    # Validators
    _validate_strace_results = validator("strace_results", allow_reuse=True)(_validate_file_path)

    @validator("certificates", pre=True, allow_reuse=True)
    @classmethod
    def _validate_certificates(cls, certificates: list[dict[str, str]]) -> dict[str, Path]:
        certificate_map: dict[str, Path] = {}

        for certificate in certificates:
            c = Certificate(**certificate)
            certificate_map[c.url] = c.certificate

        return certificate_map

    @validator("strace_regex_excludes", pre=True, each_item=True, allow_reuse=True)
    @classmethod
    def _validate_regex_list(cls, regex: str) -> re.Pattern:
        try:
            return re.compile(regex)
        except re.error as e:
            raise ValueError(f"Invalid regular expression: {regex}") from e

    def strace_files(self) -> set[str]:
        if not self._strace_files:
            parsed_strace_files = strace.get_files(self.strace_results)

            if self.strace_regex_excludes:
                self._strace_files = {
                    file
                    for file in parsed_strace_files
                    if not any(bool(re.search(exclude, file)) for exclude in self.strace_regex_excludes)
                }
                self.cache_dir.mkdir(parents=True, exist_ok=True)
                with self.cache_dir.joinpath("filtered_strace_results.txt").open("w") as output:
                    output.writelines([f"{file}\n" for file in self._strace_files])
            else:
                self._strace_files = parsed_strace_files

        return self._strace_files


class PackageData:
    """Class containing frequently accessed information and the originating package.

    Members:
        - name
        - package
        - version
    """

    def __init__(self, package: dict[str, Any]) -> None:
        """Create an instance of PackageDetails.

        Arguments:
            package: The originating package dictionary
        """
        self.package = package
        self.name: str = package["name"]
        self.version: str = package["version"]["@ver"]
        self.release: str = package["version"]["@rel"]
        self.release_version = f"{self.version}-{self.release}"

    def __eq__(self, other: object) -> bool:
        match other:
            case PackageData():
                return self.name == other.name and self.version == other.version
            case {"name": _, "version": {"@ver": _}}:
                return self.name == str(other["name"]) and self.version == str(other["version"]["@ver"])
            case _:
                return False

    def __hash__(self) -> int:
        sha = sha256()
        sha.update(self.name.encode())
        sha.update(self.version.encode())
        return int(sha.hexdigest(), 16)

    def __str__(self) -> str:
        return f"{self.name}-{self.release_version}"

    def get_source_package(self) -> str | None:
        """Get the source package from the originating package.

        Returns:
            str | None: The source package string if it is found
        """
        source_package: str | None = jmespath.search(expression='format."rpm:sourcerpm"', data=self.package)
        return source_package
