"""Choppr refines the components in a Software Bill of Materials (SBOM).

It does not replace SBOM generation tools.  Mainly, Choppr analyses a build or runtime to verify
which components are used, and remove the SBOM components not used.  Starting with file accesses, it
works backwards from how an SBOM generation tool typically would.  For example SBOM generators use
the yum database to determine which packages yum installed.  Choppr looks at all the files accessed
and queries sources like yum to determine the originating package.
"""

from __future__ import annotations

import gzip
import json
import lzma
import shutil

from collections import OrderedDict
from copy import deepcopy
from http import HTTPStatus
from typing import TYPE_CHECKING, Any, Final, cast

import jmespath
import xmltodict

from hoppr import (
    BomAccess,
    ComponentType,
    HopprLoadDataError,
)
from hoppr.base_plugins.hoppr import HopprPlugin, hoppr_process
from hoppr.models.credentials import Credentials
from hoppr.models.types import PurlType
from hoppr.result import Result
from hoppr_cyclonedx_models.cyclonedx_1_5 import Scope
from pydantic import HttpUrl, SecretStr, parse_obj_as
from requests.auth import HTTPBasicAuth

from choppr import __version__, utils
from choppr.decorators import limit_recursion
from choppr.types import ChopprConfig, PackageData
from choppr.utils import http_get


if TYPE_CHECKING:
    from pathlib import Path

    from hoppr import Component, HopprContext, Sbom
    from hoppr.models.manifest import Repository


__all__ = ["ChopprPlugin"]
__author__ = "Jesse Boswell <jesse.a.boswell@lmco.com>"
__copyright__ = "Copyright (C) 2024 Lockheed Martin Corporation"
__license__ = "MIT License"


HEADER_LENGTH = 100
RECURSION_LIMIT = 10


def _rpm_requires(data: PackageData) -> set[str] | None:
    if requirements := jmespath.search(
        expression='format."rpm:requires"."rpm:entry"[*]."@name"',
        data=data.package,
    ):
        return set(requirements)

    return None


class ChopprPlugin(HopprPlugin):
    """Plugin implementation of Choppr to integrate with Hoppr."""

    supported_purl_types = ["rpm"]  # noqa: RUF012
    products = []  # noqa: RUF012
    bom_access = BomAccess.FULL_ACCESS

    def get_version(self) -> str:  # noqa: PLR6301
        """Return the version of the Choppr plugin.

        Returns:
            str: Plugin version
        """
        return __version__

    def __init__(self, context: HopprContext, config: dict[Any, Any] | None = None) -> None:
        """Initialize plugin with Hoppr framework arguments (context and config).

        Arguments:
            context: Hoppr context
            config: Hoppr configuration
        """
        super().__init__(context, config)

        self.log = self.get_logger()
        self.log.flush_immed = True

        # Parse configuration
        self.valid_config = False
        try:
            self.plugin_config = ChopprConfig.parse_obj(config)
            self.valid_config = True
        except (HopprLoadDataError, FileNotFoundError) as e:
            self.log.error(f"Invalid Configuration: {e}")  # noqa: TRY400

        self.file_provided_by: dict[str, str] = {}
        self.repositories: dict[str, OrderedDict[str, Any]] = {}
        self.potential_packages: set[PackageData] = set()
        self.required_packages: set[PackageData] = set()
        self.nested_dependencies: set[PackageData] = set()

    def _log_header(self, title: str) -> None:
        self.log.info(f" {title} ".center(HEADER_LENGTH, "="))

    ################################################################################################
    # Pre-Stage Process
    ################################################################################################

    @hoppr_process
    def pre_stage_process(self) -> Result:
        """Collect repository information.

        Returns:
            Result: Skip if the config is invalid, or the modified SBOM
        """
        if not self.valid_config:
            return Result.skip()

        self._log_header("Populate Repositories")
        self._populate_repositories()

        self._log_header("Populate Potential Packages")
        self._populate_potential_packages()

        self._log_header("Populate Required Packages")
        self._populate_required_packages()

        self._log_header("Populate Nested Dependencies")
        self._populate_all_nested_dependencies()

        self._log_header("Filter SBOM")
        choppd_sbom = self._filter_sbom()

        return Result.success(return_obj=choppd_sbom)

    def _populate_repositories(self) -> None:
        rpm_repositories: list[Repository] = self.context.repositories[PurlType.RPM]
        expected = len(rpm_repositories)
        self.log.info(f"Pulling {expected} RPM repositories...")

        for repo in rpm_repositories:
            repo_url: str = repo.url
            if repo_return := self._get_remote_repo(repo_url):
                repo_checksum, repo_data = repo_return
                self.repositories[repo_checksum] = repo_data

        match actual := len(self.repositories):
            case _ if actual == expected:
                self.log.info("Successfully pulled all repositories")
            case 0:
                self.log.error("Failed to pull any repositories")
            case _:
                self.log.error(f"Failed to pull {expected - actual}/{expected} repositories")

    def _get_remote_repo(self, repo_url: str) -> tuple[str, OrderedDict[str, Any]] | None:
        repositories_dir: Final[Path] = self.plugin_config.cache_dir / "repositories"
        repositories_dir.mkdir(parents=True, exist_ok=True)

        url = parse_obj_as(HttpUrl, f"{repo_url}/repodata/repomd.xml")
        self.log.info(f"Pulling repository data from {url}...")

        basic_auth = None
        credentials = Credentials.find(url.host)
        verify = self.plugin_config.certificates.get(url.host, True)
        if credentials is not None and isinstance(credentials.password, SecretStr):
            self.log.debug(f"Found credentials for host: {url.host}")
            basic_auth = HTTPBasicAuth(username=credentials.username, password=credentials.password.get_secret_value())
        else:
            self.log.debug(f"No credentials for host: {url.host}")

        with http_get(url, basic_auth, verify) as response:
            if response.status_code > HTTPStatus.MULTIPLE_CHOICES:
                self.log.error(f"Failed to pull repository: {response.content.decode()}")
                return None

            repomd_dict = xmltodict.parse(response.content)

            primary_checksum = jmespath.search(
                expression='[?"@type"==\'primary\'].checksum."#text"',
                data=repomd_dict["repomd"]["data"],
            )

            # Check if repository is cached
            if (
                isinstance(primary_checksum, list)
                and len(primary_checksum) == 1
                and repositories_dir.joinpath(f"{primary_checksum[0]}.json").is_file()
            ):
                with repositories_dir.joinpath(f"{primary_checksum[0]}.json").open() as file:
                    self.log.info("Successfully loaded repository from cache")
                    return (primary_checksum[0], cast(OrderedDict[str, Any], json.load(file)))

            primary_location = jmespath.search(
                expression='[?"@type"==\'primary\'].location."@href"',
                data=repomd_dict["repomd"]["data"],
            )

            if (
                isinstance(primary_location, list)
                and len(primary_location) == 1
                and isinstance(primary_checksum, list)
                and len(primary_checksum) == 1
            ):
                primary_url = parse_obj_as(HttpUrl, f"{repo_url}/{primary_location[0]}")
                with http_get(primary_url, basic_auth, verify) as primary_response:
                    if primary_response.status_code > HTTPStatus.MULTIPLE_CHOICES:
                        self.log.error(f"Failed to pull repository: {response.content.decode()}")
                        return None

                    match primary_url:
                        case _ if primary_url.endswith(".gz"):
                            primary_xml = gzip.decompress(primary_response.content)
                        case _ if primary_url.endswith(".xz"):
                            primary_xml = lzma.decompress(primary_response.content)
                        case _ if primary_url.endswith(".xml"):
                            primary_xml = primary_response.content
                        case _:
                            self.log.error(f"Unsupported file type found for primary repository: {primary_url}")
                            return None

                    self.log.info("Successfully pulled repository data")

                    repo_data = xmltodict.parse(primary_xml)
                    # Write repository data to cache
                    with repositories_dir.joinpath(f"{primary_checksum[0]}.json").open("w") as file:
                        json.dump(repo_data, file, indent=4)
                    return (primary_checksum[0], repo_data)
            else:
                self.log.error("Failed to get `location` and `checksum` from repository")

        return None

    def _populate_potential_packages(self, required_files: set[str] | None = None) -> None:
        using_strace_files = False

        if not required_files:
            using_strace_files = True
            required_files = self.plugin_config.strace_files()

        for file in required_files:
            self.log.debug(f"Getting RPM associated with file: {file}")

            if packages := self._rpm_provides(file, using_strace_files):
                for package in packages:
                    self.log.debug(f"Successfully found RPM: {package.name}")

                    self.potential_packages.add(package)
                    self.file_provided_by[file] = str(package)

        self.log.info(f"Found {len(self.potential_packages)} potential packages")

        (self.plugin_config.cache_dir / "potential_packages.txt").write_text(
            "\n".join(self.potential_packages), "utf-8"
        )

    def _rpm_provides(self, filename: str, search_files: bool = False, arch: str = "x86_64") -> set[PackageData]:
        if filename in self.file_provided_by:
            return set()

        providers: set[PackageData] = set()

        for repo in self.repositories.values():
            package_data: set[PackageData] = {
                PackageData(package)
                for package in jmespath.search(
                    expression=f"metadata.package[?arch == '{arch}' || arch == 'noarch']",
                    data=repo,
                )
            }
            if search_files:
                providers.update(utils.RPM.search_files_section(filename, package_data))

            if not providers:
                providers.update(utils.RPM.search_rpm_provides_section(filename, package_data))

        return providers

    def _populate_required_packages(self) -> None:
        for component in self.context.delivered_sbom.components or []:
            if package := self._get_component_package(component):
                self.log.debug(f"Package required: {package}")
                self.required_packages.add(package)

        self.log.info(f"Found {len(self.required_packages)} required packages")

        (self.plugin_config.cache_dir / "required_packages.txt").write_text(
            data="\n".join(self.required_packages),
            encoding="utf-8",
        )

    def _populate_all_nested_dependencies(self) -> None:
        for package in deepcopy(self.required_packages):
            if package_requires := _rpm_requires(package):
                self.log.debug(f"Getting nested dependencies for {package}")
                amount_before = len(self.nested_dependencies)
                self._populate_nested_dependencies(package_requires)
                self.log.debug(f"Found {len(self.nested_dependencies) - amount_before} new nested dependencies")

        self.log.info(f"Found {len(self.nested_dependencies)} nested dependencies")

        (self.plugin_config.cache_dir / "nested_dependencies.txt").write_text(
            "\n".join(self.nested_dependencies), "utf-8"
        )

    @limit_recursion(RECURSION_LIMIT)
    def _populate_nested_dependencies(self, required_files: set[str]) -> None:
        for file in required_files:
            if packages := {
                package
                for package in self._rpm_provides(file)
                if package not in self.required_packages | self.nested_dependencies
            }:
                for package in packages:
                    self.nested_dependencies.add(package)

                    if package_requires := _rpm_requires(package):
                        self._populate_nested_dependencies(package_requires)

    def _get_component_package(self, component: Component) -> PackageData | None:
        if not component.version:
            return None
        return next(
            (package for package in self.potential_packages if str(package) == f"{component.name}-{component.version}"),
            None,
        )

    def _filter_sbom(self) -> Sbom:
        choppd_sbom = self.context.delivered_sbom.copy(deep=True)
        choppd_sbom.components.clear()

        for component in self.context.delivered_sbom.components or []:
            if scope := self._get_component_scope(component):
                component.scope = scope
                match scope:
                    case Scope.REQUIRED:
                        self.log.debug("Package is required")
                        choppd_sbom.components.append(component)
                    case Scope.EXCLUDED:
                        self.log.error("Package is not required")
            else:
                self.log.warning("Unable to determin if package is required")

        if self.plugin_config.delete_excluded:
            self.log.debug("Deleted excluded components")
            return choppd_sbom
        return self.context.delivered_sbom

    def _get_component_scope(self, component: Component) -> Scope | None:
        self.log.debug(f"Checking if package is required: {component.name}-{component.version}")
        if str(component.type) == str(ComponentType.OPERATING_SYSTEM):
            return Scope.REQUIRED
        if not component.version:
            return None
        return next(
            (
                Scope.REQUIRED
                for package in self.required_packages | self.nested_dependencies
                if str(package) == f"{component.name}-{component.version}"
            ),
            Scope.EXCLUDED,
        )

    ################################################################################################
    # Post-Stage Process
    ################################################################################################

    @hoppr_process
    def post_stage_process(self) -> Result:
        """Perform cleanup tasks after running Choppr.

        Returns:
            Result: The updated SBOM with the unused packages removed
        """
        if not self.valid_config:
            return Result.skip()

        if self.plugin_config.clear_cache:
            shutil.rmtree(self.plugin_config.cache_dir)

        return Result.success()
