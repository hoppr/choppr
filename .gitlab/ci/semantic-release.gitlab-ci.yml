---
variables:
  SEMANTIC_RELEASE_IMAGE: hoppr/semantic-release
  SEMANTIC_RELEASE_TAG: "0.0.52"
  GIT_AUTHOR_NAME: $GITLAB_USER_LOGIN
  GIT_AUTHOR_EMAIL: $GITLAB_USER_EMAIL
  GIT_COMMITTER_NAME: $GITLAB_USER_LOGIN
  GIT_COMMITTER_EMAIL: $GITLAB_USER_EMAIL
  GIT_CREDENTIALS: gitlab-ci-token:${GITLAB_CI_TOKEN}
  GITLAB_TOKEN: $GITLAB_CI_TOKEN
  ENABLE_SEMANTIC_V_SYNTAX: "true"
  SEMANTIC_RELEASE_BRANCH: main

.semantic-base:
  image:
    name: $SEMANTIC_RELEASE_IMAGE:$SEMANTIC_RELEASE_TAG
    entrypoint: [""]
  before_script:
    - |
      echo "GIT_AUTHOR_NAME:     $GIT_AUTHOR_NAME"
      echo "GIT_AUTHOR_EMAIL:    $GIT_AUTHOR_EMAIL"
      echo "GIT_COMMITTER_NAME:  $GIT_COMMITTER_NAME"
      echo "GIT_COMMITTER_EMAIL: $GIT_COMMITTER_EMAIL"
    - yq --version
  cache:
    key: $CI_COMMIT_REF_SLUG
    policy: pull-push
    paths:
      - node_modules

semantic-release:dry-run:
  extends: .semantic-base
  stage: pre
  needs: []
  resource_group: release
  environment: release
  variables:
    ADDITIONAL_NPM_PKGS: ""
  script:
    - | # Update .releaserc.yml to generate dev release version if not running on main/dev
      if ! [[ "$CI_COMMIT_REF_NAME" =~ "^(main|dev)$" ]]; then
        filter="$(printf '.branches += {"name": "%s", "channel": "dev"}' "$CI_COMMIT_REF_NAME")"
        yq eval --inplace --prettyPrint "$filter" .releaserc.yml
      fi
    - | # If running on a fork, bypass Semantic Release
      if [[ "$CI_PROJECT_ROOT_NAMESPACE" == "hoppr" ]]; then
        semantic-release --dry-run --no-ci
      else
        echo RELEASE_VERSION=v0.0.0 > release.env
      fi
    - cat release.env 2> /dev/null || echo "No release was created, check your commit message."
  rules:
    - if: $ENABLE_SEMANTIC_RELEASE_DRY_RUN == "true"
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
  allow_failure: false
  artifacts:
    paths:
      - .releaserc.yml
      - release.env
    reports:
      dotenv: release.env

semantic-release:
  extends: .semantic-base
  stage: deploy
  environment:
    name: Production
    deployment_tier: production
  needs:
    - job: build-image:release
      artifacts: false
      optional: true
    - job: poetry
      artifacts: false
    - job: build-dist
      artifacts: false
    - job: semantic-release:dry-run
      artifacts: false
  script:
    - semantic-release
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_REF_NAME =~ /^main$/
      when: on_success
  artifacts:
    paths:
      - choppr/__init__.py
      - pyproject.toml
      - docs/*

semantic-release:dev:
  extends: semantic-release
  environment:
    name: Dev
    deployment_tier: development
  rules:
    - if: $CI_COMMIT_REF_NAME =~ /^dev$/
      when: on_success
